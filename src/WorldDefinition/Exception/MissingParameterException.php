<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition\Exception;

class MissingParameterException extends \Exception
{

}