<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition;

use GameOfLife\WorldDefinition\Exception\InvalidWorldDefinitionException;

class WorldDefinitionValidator
{
    /**
     * @throws InvalidWorldDefinitionException
     */
    public function validate(WorldDefinition $worldDefinition): void
    {
        if ($worldDefinition->getCells() < 1) {
            throw new InvalidWorldDefinitionException('Invalid xml attribute - cells');
        }

        if ($worldDefinition->getSpecies() < 1) {
            throw new InvalidWorldDefinitionException('Invalid xml attribute - species');
        }

        if ($worldDefinition->getIterations() < 1) {
            throw new InvalidWorldDefinitionException('Invalid xml attribute - iterations');
        }

        $occupiedCells = [];
        foreach ($worldDefinition->getOrganismList()->getOrganisms() as $organism) {
            if ($organism->getXPos() >= $worldDefinition->getCells()) {
                throw new InvalidWorldDefinitionException('Invalid xml attribute - x-pos');
            }
            if ($organism->getYPos() >= $worldDefinition->getCells()) {
                throw new InvalidWorldDefinitionException('Invalid xml attribute - y-pos');
            }
            if (isset($occupiedCells[$organism->getXPos()][$organism->getYPos()])) {
                throw new InvalidWorldDefinitionException('Invalid xml - multiple organisms with same position');
            }
            $occupiedCells[$organism->getXPos()][$organism->getYPos()] = 1;
        }
    }
}

