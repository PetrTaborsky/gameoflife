<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition;

use GameOfLife\Organism\OrganismList;

class WorldDefinition
{
    private int $dimension;

    private int $numberOfSpecieTypes;

    private int $iterations;

    private OrganismList $organismList;

    public function __construct(int $dimension, int $numberOfSpeciesTypes, int $iterations, OrganismList $organismList)
    {
        $this->dimension = $dimension;
        $this->numberOfSpecieTypes = $numberOfSpeciesTypes;
        $this->iterations = $iterations;
        $this->organismList = $organismList;
    }

    public function getCells(): int
    {
        return $this->dimension;
    }

    public function getSpecies(): int
    {
        return $this->numberOfSpecieTypes;
    }

    public function getIterations(): int
    {
        return $this->iterations;
    }

    public function getOrganismList(): OrganismList
    {
        return $this->organismList;
    }

}
