<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition;

use GameOfLife\Organism\Organism;
use GameOfLife\Organism\OrganismList;
use GameOfLife\Organism\OrganismFactory;
use GameOfLife\Organism\SpeciesEnum;
use GameOfLife\WorldDefinition\Exception\MissingParameterException;

class WorldDefinitionDeserializer
{

    /**
     * @throws \Exception
     * @throws MissingParameterException
     */
    public function deserializeIntoWorldDefinition(string $xmlString): WorldDefinition
    {
        $xmlObject = new \SimpleXMLElement($xmlString);
        $dimension = (int)$this->parseParameter($xmlObject, 'world->cells');
        $numberOfSpecieTypes = (int)$this->parseParameter($xmlObject, 'world->species');
        $iterations = (int)$this->parseParameter($xmlObject, 'world->iterations');
        $organismList = $this->parseOrganismList($xmlObject);

        return new WorldDefinition($dimension, $numberOfSpecieTypes, $iterations, $organismList);
    }

    /**
     * @throws MissingParameterException
     */
    private function parseOrganismList(\SimpleXMLElement $xmlObject): OrganismList
    {
        $xmlOrganismList = $this->parseParameter($xmlObject, 'organisms->organism');
        $organisms = [];
        foreach ($xmlOrganismList as $xmlOrganism) {
            $organisms[] = $this->parseOrganism($xmlOrganism);
        }
        return new OrganismList($organisms);
    }

    /**
     * @throws MissingParameterException
     */
    private function parseOrganism(\SimpleXMLElement $xmlOrganism): Organism
    {
        $xPos = (int)$this->parseParameter($xmlOrganism, 'x_pos');
        $yPos = (int)$this->parseParameter($xmlOrganism, 'y_pos');
        $species = (int)$this->parseParameter($xmlOrganism, 'species');
        $speciesType = SpeciesEnum::create($species);
        return OrganismFactory::createOrganism($xPos, $yPos, $speciesType);
    }

    /**
     * @throws MissingParameterException
     */
    private function parseParameter(\SimpleXMLElement $xmlObject, string $path): \SimpleXMLElement
    {
        foreach (explode('->', $path) as $pathPart) {
            $xmlObject = $xmlObject->{$pathPart};
        }

        if ($xmlObject->count() === 0) {
            throw new MissingParameterException(sprintf('Missing xml attribute - %s', $path));
        }

        return $xmlObject;
    }
}
