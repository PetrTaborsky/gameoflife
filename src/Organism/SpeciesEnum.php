<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

use Enum\AbstractEnum;

class SpeciesEnum extends AbstractEnum
{
    public const TYPE_RED = 1;
    public const TYPE_GREEN = 2;
    public const TYPE_BLUE = 3;
}