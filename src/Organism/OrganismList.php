<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

class OrganismList
{
    /**
     * @var Organism[] $organisms
     */
    private array $organisms;

    /**
     * @param Organism[] $organisms
     */
    public function __construct(array $organisms)
    {
        $this->organisms = $organisms;
    }

    /**
     * @return Organism[]
     */
    public function getOrganisms(): array
    {
        return $this->organisms;
    }

    /**
     * @return Organism[]
     */
    public function getOrganismsByType(SpeciesEnum $type): OrganismList
    {
        $organismsByType = [];
        foreach ($this->getOrganisms() as $organism) {
            if ($organism->getType()->getValue() === $type->getValue()) {
                $organismsByType[] = $organism;
            }
        }
        return new self($organismsByType);
    }

    /**
     * @return Organism[]
     */
    public function getOrganismsByQuantity(int $quantity): OrganismList
    {
        $organismsByType = $organismsByTypeHavingQuantity = [];
        foreach ($this->getOrganisms() as $organism) {
            $organismsByType[$organism->getType()->getValue()][] = $organism;
        }

        foreach ($organismsByType as $organisms) {
            if (count($organisms) === $quantity) {
                $organismsByTypeHavingQuantity[] = array_pop($organisms);
            }
        }

        return new self($organismsByTypeHavingQuantity);
    }

    public function getOrganismsInhabitingPosition(int $xPos, int $yPos): self
    {
        $inhabitingOrganisms = [];
        foreach ($this->getOrganisms() as $organism) {
            if ($organism->inhabitsPosition($xPos, $yPos)) {
                $inhabitingOrganisms[] = $organism;
            }
        }
        return new self($inhabitingOrganisms);
    }

    public function getOrganismsBorderingPosition(int $xPos, int $yPos): self
    {
        $borderingOrganisms = [];
        foreach ($this->getOrganisms() as $organism) {
            if ($organism->bordersPosition($xPos, $yPos)) {
                $borderingOrganisms[] = $organism;
            }
        }
        return new self($borderingOrganisms);
    }
}
