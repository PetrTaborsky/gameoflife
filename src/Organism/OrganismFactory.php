<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

use GameOfLife\Organism\Exception\InvalidCoordinatesException;

class OrganismFactory
{
    /**
     * @throws InvalidCoordinatesException
     */
    public static function createOrganism(int $xPos, int $yPos, SpeciesEnum $type): Organism
    {
        if ($xPos < 0 || $yPos < 0) {
            throw new InvalidCoordinatesException();
        }

        return new Organism($xPos, $yPos, $type);
    }
}
