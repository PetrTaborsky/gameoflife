<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

class Organism
{
    private int $xPos;

    private int $yPos;

    private SpeciesEnum $type;

    public function __construct(int $xPos, int $yPos, SpeciesEnum $type)
    {
        $this->xPos = $xPos;
        $this->yPos = $yPos;
        $this->type = $type;
    }

    public function getXPos(): int
    {
        return $this->xPos;
    }

    public function getYPos(): int
    {
        return $this->yPos;
    }

    public function getType(): SpeciesEnum
    {
        return $this->type;
    }

    public function bordersPosition(int $xPos, int $yPos): bool
    {
        if (abs($this->getXPos() - $xPos) > 1) {
            return false;
        }
        if (abs($this->getYPos() - $yPos) > 1) {
            return false;
        }
        if ($this->inhabitsPosition($xPos, $yPos)) {
            return false;
        }
        return true;
    }

    public function inhabitsPosition(int $xPos, int $yPos): bool
    {
        if ($this->getXPos() === $xPos && $this->getYPos() === $yPos) {
            return true;
        }
        return false;
    }
}
