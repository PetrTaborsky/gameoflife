<?php

declare(strict_types=1);

namespace GameOfLife\File;

use GameOfLife\File\Exception\FileNotExistsException;
use GameOfLife\File\Exception\FileReadFailedException;

class FileReader
{
    /**
     * @throws FileNotExistsException
     * @throws FileReadFailedException
     */
    public function getFileContents(string $filePath): string
    {
        if (file_exists($filePath) === false) {
            throw new FileNotExistsException(sprintf('File %s does not exist', $filePath));
        }
        $fileContent = file_get_contents($filePath);
        if ($fileContent === false) {
            throw new FileReadFailedException(sprintf('File %s does not exist', $filePath));
        }
        return $fileContent;
    }
}
