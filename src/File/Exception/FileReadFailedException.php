<?php

declare(strict_types=1);

namespace GameOfLife\File\Exception;

class FileReadFailedException extends \Exception
{

}