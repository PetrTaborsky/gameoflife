<?php

declare(strict_types=1);

namespace GameOfLife\File\Exception;

class FileNotExistsException extends \Exception
{

}