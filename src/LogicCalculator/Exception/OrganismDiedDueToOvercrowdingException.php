<?php

declare(strict_types=1);

namespace GameOfLife\Organism\Exception;

class OrganismDiedDueToOvercrowdingException extends \Exception
{

}