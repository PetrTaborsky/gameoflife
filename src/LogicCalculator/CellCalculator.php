<?php

declare(strict_types=1);

namespace GameOfLife\LogicCalculator;

use GameOfLife\Organism\Exception\EmptyCellResultException;
use GameOfLife\Organism\Exception\EmptyCellStaysEmptyException;
use GameOfLife\Organism\Exception\OrganismDiedDueToIsolationException;
use GameOfLife\Organism\Exception\OrganismDiedDueToOvercrowdingException;
use GameOfLife\Organism\Organism;
use GameOfLife\Organism\OrganismFactory;
use GameOfLife\Organism\OrganismList;

class CellCalculator
{
    /**
     * @throws EmptyCellResultException
     */
    public function calculateCellResult(OrganismList $organismList, int $xPos, int $yPos): Organism
    {
        $inhabitingOrganismList = $organismList->getOrganismsInhabitingPosition($xPos, $yPos);
        $borderingOrganismsList = $organismList->getOrganismsBorderingPosition($xPos, $yPos);

        try {
            if ($inhabitingOrganismList->getOrganisms() !== []) {
                return $this->calculateCellResultForInhabitedCell($inhabitingOrganismList, $borderingOrganismsList);
            } else {
                return $this->calculateCellResultForEmptyCell($borderingOrganismsList, $xPos, $yPos);
            }
        } catch (OrganismDiedDueToIsolationException | OrganismDiedDueToOvercrowdingException | EmptyCellStaysEmptyException $e) {
            throw new EmptyCellResultException();
        }
    }

    /**
     * @throws EmptyCellStaysEmptyException
     */
    private function calculateCellResultForEmptyCell(OrganismList $borderingOrganismsList, int $xPos, int $yPos): Organism
    {
        //not exactly 3 of same species => cell remains empty
        $organismsToInhabitCell = $borderingOrganismsList->getOrganismsByQuantity(3)->getOrganisms();
        if ($organismsToInhabitCell === []) {
            throw new EmptyCellStaysEmptyException();
        }

        //exactly 3 of same species => new organism is born with type chosen randomly
        $newOrganismType = $this->chooseRandomOrganism($organismsToInhabitCell)->getType();
        return OrganismFactory::createOrganism($xPos, $yPos, $newOrganismType);
    }

    /**
     * @throws OrganismDiedDueToIsolationException
     * @throws OrganismDiedDueToOvercrowdingException
     */
    private function calculateCellResultForInhabitedCell(OrganismList $inhabitingOrganismList, OrganismList $borderingOrganismsList): Organism
    {
        $inhabitingOrganism = $inhabitingOrganismList->getOrganisms()[0];
        $borderingOrganismsOfSameTypeCount = count($borderingOrganismsList->getOrganismsByType($inhabitingOrganism->getType())->getOrganisms());

        //(-∞,1> of same species => dies
        if ($borderingOrganismsOfSameTypeCount <= 1) {
            throw new OrganismDiedDueToIsolationException();
        }
        //<4,∞> of same species =>dies
        if ($borderingOrganismsOfSameTypeCount >= 4) {
            throw new OrganismDiedDueToOvercrowdingException();
        }
        //<2,3> of same species => no change
        return $inhabitingOrganism;
    }

    /**
     * @param Organism[]
     */
    private function chooseRandomOrganism(array $organisms): Organism
    {
        return $organisms[array_rand($organisms)];
    }
}
