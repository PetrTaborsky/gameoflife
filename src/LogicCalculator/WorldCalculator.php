<?php

declare(strict_types=1);

namespace GameOfLife\LogicCalculator;

use GameOfLife\Organism\Exception\EmptyCellResultException;
use GameOfLife\Organism\OrganismList;
use GameOfLife\WorldDefinition\WorldDefinition;

class WorldCalculator
{
    //TODO make (at least) this available as a service
    private function calculateNextStep(OrganismList $organismList, int $dimension): OrganismList
    {
        $cellCalculator = new CellCalculator();
        $nextStepOrganisms = [];
        for ($xPos = 0; $xPos < $dimension; $xPos++) {
            for ($yPos = 0; $yPos < $dimension; $yPos++) {
                try {
                    $organism = $cellCalculator->calculateCellResult($organismList, $xPos, $yPos);
                    $nextStepOrganisms[] = $organism;
                } catch (EmptyCellResultException $e) {
                    //nothing to do here, cell just remains empty
                }
            }
        }
        return new OrganismList($nextStepOrganisms);
    }

    /**
     * @return OrganismList[]
     */
    public function calculateResult(WorldDefinition $worldDefinition): array
    {
        $organismList = $worldDefinition->getOrganismList();
        $dimension = $worldDefinition->getCells();
        //TODO create new entity for further use, not array
        $organismListSteps = [];
        for ($iteration = 0; $iteration < $worldDefinition->getIterations(); $iteration++) {
            //TODO condition to end unnecessary iterations can be implemented when two organismLists in a row are the same, which means no other changes will occur
            $organismList = $this->calculateNextStep($organismList, $dimension);
            $organismListSteps[] = $organismList;
        }
        return $organismListSteps;
    }
}
