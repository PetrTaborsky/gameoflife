<?php

declare(strict_types=1);

namespace GameOfLife\File;

use GameOfLife\File\Exception\FileNotExistsException;
use PHPUnit\Framework\TestCase;

class FileReaderTest extends TestCase
{
    private FileReader $fileReader;

    public function setUp(): void
    {
        $this->fileReader = new FileReader();
        parent::setUp();
    }

    /**
     * @dataProvider getFileContentsProvider
     */
    public function testGetFileContents(string $filePath, string $fileContents): void
    {
        $this->assertEquals($this->fileReader->getFileContents($filePath), $fileContents);
    }

    public function testGetFileContentsException(): void
    {
        $this->expectException(FileNotExistsException::class);
        $this->fileReader->getFileContents('not existing path');
    }

    /**
     * @return string[][]
     */
    public function getFileContentsProvider(): array
    {
        return [
            'Empty File' => [__DIR__ . '/Sample/emptyFile', ''],
            'Not Empty File' => [__DIR__ . '/Sample/notEmptyFile', 'file_content']
        ];
    }
}