<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition;

use PHPUnit\Framework\TestCase;
use GameOfLife\WorldDefinition\Exception\MissingParameterException;

class WorldDefinitionDeserializerTest extends TestCase
{
    private WorldDefinitionDeserializer $worldDefinitionDeserializer;

    public function setUp(): void
    {
        $this->worldDefinitionDeserializer = new WorldDefinitionDeserializer();
        parent::setUp();
    }

    /**
     * @dataProvider deserializeIntoWorldDefinitionProvider
     */
    public function testDeserializeIntoWorldDefinition(string $xmlString): void
    {
        $worldDefinition = $this->worldDefinitionDeserializer->deserializeIntoWorldDefinition($xmlString);
        $organisms = $worldDefinition->getOrganismList()->getOrganisms();
        $this->assertEquals(10, $worldDefinition->getCells());
        $this->assertEquals(2, $worldDefinition->getSpecies());
        $this->assertEquals(100, $worldDefinition->getIterations());
        $this->assertCount(2, $organisms);
        $this->assertEquals(4, $organisms[1]->getXPos());
        $this->assertEquals(1, $organisms[1]->getYPos());
        $this->assertEquals(2, $organisms[1]->getType()->getValue());
    }

    /**
     * @return string[][]
     */
    public function deserializeIntoWorldDefinitionProvider(): array
    {
        return [
            'Valid xml definition' => [file_get_contents(__DIR__ . '/Sample/validXmlString')]
        ];
    }

    /**
     * @dataProvider deserializeIntoWorldDefinitionExceptionProvider
     */
    public function testDeserializeIntoWorldDefinitionExceptions(string $xmlString, string $exceptionName): void
    {
        $this->expectException($exceptionName);
        $this->worldDefinitionDeserializer->deserializeIntoWorldDefinition($xmlString);
    }


    /**
     * @return string[][]
     */
    public function deserializeIntoWorldDefinitionExceptionProvider(): array
    {
        return [
            'Missing attribute xml definition' => [file_get_contents(__DIR__ . '/Sample/missingAttributeXmlString'), MissingParameterException::class]
        ];
    }
}