<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

use PHPUnit\Framework\TestCase;

class OrganismListTest extends TestCase
{
    /**
     * @dataProvider getOrganismsByTypeProvider
     */
    public function testGetOrganismsByType(OrganismList $organismList, SpeciesEnum $type, int $organismCount): void
    {
        $this->assertCount($organismCount, $organismList->getOrganismsByType($type)->getOrganisms());
    }

    /**
     * @return string[][]
     */
    public function getOrganismsByTypeProvider(): array
    {
        $organismRed = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_RED));
        $organismGreen = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_GREEN));
        $organismBlue = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_BLUE));

        $organismListEmpty = new OrganismList([]);
        $organismList = new OrganismList([
            $organismRed, $organismRed, $organismRed, $organismRed,
            $organismGreen, $organismGreen, $organismGreen,
            $organismBlue, $organismBlue,
        ]);
        return [
            'Empty list' => [$organismListEmpty, SpeciesEnum::create(SpeciesEnum::TYPE_RED), 0],
            'Red count check' => [$organismList, SpeciesEnum::create(SpeciesEnum::TYPE_RED), 4],
            'Green count check' => [$organismList, SpeciesEnum::create(SpeciesEnum::TYPE_GREEN), 3],
            'Blue count check' => [$organismList, SpeciesEnum::create(SpeciesEnum::TYPE_BLUE), 2],
        ];
    }

    /**
     * @dataProvider getOrganismsByQuantityProvider
     */
    public function testGetOrganismsByQuantity(OrganismList $organismList, int $requiredQuantity, int $expectedOrganismCount): void
    {
        $this->assertCount($expectedOrganismCount, $organismList->getOrganismsByQuantity($requiredQuantity)->getOrganisms());
    }

    /**
     * @return string[][]
     */
    public function getOrganismsByQuantityProvider(): array
    {
        $organismRed = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_RED));
        $organismGreen = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_GREEN));
        $organismBlue = OrganismFactory::createOrganism(0,1, SpeciesEnum::create(SpeciesEnum::TYPE_BLUE));

        $organismListEmpty = new OrganismList([]);
        $organismList1 = new OrganismList([$organismRed, $organismGreen]);
        $organismList2 = new OrganismList([
            $organismRed, $organismRed, $organismRed,
            $organismGreen, $organismGreen, $organismGreen,
            $organismBlue,
        ]);
        return [
            'Empty list' => [$organismListEmpty, 3, 0],
            'Quantity 1' => [$organismList1, 1, 2],
            'Quantity 2' => [$organismList2, 2, 0],
            'Quantity 3' => [$organismList2, 3, 2],
        ];
    }

    /**
     * @dataProvider getOrganismsInhabitingPositionProvider
     */
    public function testGetOrganismsInhabitingPosition(OrganismList $organismList, int $xPos, int $yPos, int $expectedOrganismCount): void
    {
        $this->assertCount($expectedOrganismCount, $organismList->getOrganismsInhabitingPosition($xPos, $yPos)->getOrganisms());
    }

    /**
     * @return string[][]
     */
    public function getOrganismsInhabitingPositionProvider(): array
    {
        $organism = OrganismFactory::createOrganism(2,3, SpeciesEnum::create(SpeciesEnum::TYPE_RED));

        $organismListEmpty = new OrganismList([]);
        $organismList = new OrganismList([$organism]);

        return [
            'Empty list' => [$organismListEmpty, 2, 3, 0],
            'Quantity 1' => [$organismList, 2, 3, 1],
            'Quantity 0' => [$organismList, 2, 2, 0],
        ];
    }

    /**
     * @dataProvider getOrganismsBorderingPositionProvider
     */
    public function testGetOrganismsBorderingPosition(OrganismList $organismList, int $xPos, int $yPos, int $expectedOrganismCount): void
    {
        $this->assertCount($expectedOrganismCount, $organismList->getOrganismsBorderingPosition($xPos, $yPos)->getOrganisms());
    }

    /**
     * @return string[][]
     */
    public function getOrganismsBorderingPositionProvider(): array
    {
        $organismListEmpty = new OrganismList([]);
        $speciesType = SpeciesEnum::create(SpeciesEnum::TYPE_RED);
        $organismList = new OrganismList([
            OrganismFactory::createOrganism(1,1, $speciesType),
            OrganismFactory::createOrganism(1,2, $speciesType),
            OrganismFactory::createOrganism(2,2, $speciesType),
            OrganismFactory::createOrganism(2,3, $speciesType),
            OrganismFactory::createOrganism(3,2, $speciesType),
            OrganismFactory::createOrganism(3,3, $speciesType),
            OrganismFactory::createOrganism(4,4, $speciesType),
            OrganismFactory::createOrganism(5,5, $speciesType),
            OrganismFactory::createOrganism(6,6, $speciesType),
        ]);

        return [
            'Empty list' => [$organismListEmpty, 2, 2, 0],
            'Quantity 1' => [$organismList, 7, 7, 1],
            'Quantity 1 inh.' => [$organismList, 6, 6, 1],
            'Quantity 2 inh.' => [$organismList, 5, 5, 2],
            'Quantity 4 inh.' => [$organismList, 3, 3, 4],
            'Quantity 5 inh.' => [$organismList, 2, 2, 5],
        ];
    }
}