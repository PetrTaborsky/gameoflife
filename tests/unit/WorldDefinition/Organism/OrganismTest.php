<?php

declare(strict_types=1);

namespace GameOfLife\Organism;

use PHPUnit\Framework\TestCase;

class OrganismTest extends TestCase
{
    /**
     * @dataProvider inhabitsPositionProvider
     */
    public function testInhabitsPosition(Organism $organism, int $xPos, int $yPos, bool $inhabitsPosition): void
    {
        $this->assertEquals($organism->inhabitsPosition($xPos, $yPos), $inhabitsPosition);
    }

    /**
     * @return string[][]
     */
    public function inhabitsPositionProvider(): array
    {
        $speciesType = SpeciesEnum::create(SpeciesEnum::TYPE_RED);
        return [
            'Inhabiting position' => [OrganismFactory::createOrganism(2,3, $speciesType), 2, 3, true],
            'Not inhabiting position #1' => [OrganismFactory::createOrganism(2,3, $speciesType), 2, 2, false],
            'Not inhabiting position #2' => [OrganismFactory::createOrganism(2,3, $speciesType), 3, 3, false],
            'Not inhabiting position #3' => [OrganismFactory::createOrganism(2,3, $speciesType), 3, 2, false],
            'Not inhabiting position #4' => [OrganismFactory::createOrganism(2,3, $speciesType), 0, 0, false],
        ];
    }

    /**
     * @dataProvider bordersPositionProvider
     */
    public function testBordersPosition(Organism $organism, int $xPos, int $yPos, bool $bordersPosition): void
    {
        $this->assertEquals($organism->bordersPosition($xPos, $yPos), $bordersPosition);
    }

    /**
     * @return string[][]
     */
    public function bordersPositionProvider(): array
    {
        $speciesType = SpeciesEnum::create(SpeciesEnum::TYPE_RED);
        return [
            'Inhabiting position' => [OrganismFactory::createOrganism(2,3, $speciesType), 2, 3, false],
            'Bordering position #1' => [OrganismFactory::createOrganism(2,3, $speciesType), 2, 2, true],
            'Bordering position #2' => [OrganismFactory::createOrganism(2,3, $speciesType), 3, 3, true],
            'Bordering position #3' => [OrganismFactory::createOrganism(2,3, $speciesType), 3, 4, true],
            'Bordering position #4' => [OrganismFactory::createOrganism(2,3, $speciesType), 1, 2, true],
            'Not bordering position' => [OrganismFactory::createOrganism(2,3, $speciesType), 1, 1, false],
        ];
    }
}