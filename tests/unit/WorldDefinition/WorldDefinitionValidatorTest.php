<?php

declare(strict_types=1);

namespace GameOfLife\WorldDefinition;

use PHPUnit\Framework\TestCase;

class WorldDefinitionValidatorTest extends TestCase
{
    private WorldDefinitionDeserializer $worldDefinitionDeserializer;

    private WorldDefinitionValidator $worldDefinitionValidator;

    public function setUp(): void
    {
        $this->worldDefinitionDeserializer = new WorldDefinitionDeserializer();
        $this->worldDefinitionValidator = new WorldDefinitionValidator();
        parent::setUp();
    }

    /**
     * @dataProvider validateWorldDefinitionExceptionProvider
     */
    public function testValidateWorldDefinitionException(string $xmlString, string $exceptionMessage): void
    {
        $worldDefinition = $this->worldDefinitionDeserializer->deserializeIntoWorldDefinition($xmlString);

        $this->expectExceptionMessage($exceptionMessage);
        $this->worldDefinitionValidator->validate($worldDefinition);
    }

    /**
     * @return string[][]
     */
    public function validateWorldDefinitionExceptionProvider(): array
    {
        return [
            'Invalid cells' => [file_get_contents(__DIR__ . '/Sample/invalidCellsXmlString'), 'Invalid xml attribute - cells'],
            'Invalid iterations' => [file_get_contents(__DIR__ . '/Sample/invalidIterationsXmlString'), 'Invalid xml attribute - iterations'],
            'Invalid species' => [file_get_contents(__DIR__ . '/Sample/invalidSpeciesXmlString'), 'Invalid xml attribute - species'],
            'X-pos exceeds cells' => [file_get_contents(__DIR__ . '/Sample/xPosExceedsCellsXmlString'), 'Invalid xml attribute - x-pos'],
            'Y-pos exceeds cells' => [file_get_contents(__DIR__ . '/Sample/yPosExceedsCellsXmlString'), 'Invalid xml attribute - y-pos'],
            'Multiple organisms in same position' => [file_get_contents(__DIR__ . '/Sample/multipleOrganismsInSamePositionXmlString'), 'Invalid xml - multiple organisms with same position'],
        ];
    }
}