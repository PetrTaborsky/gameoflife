<?php

declare(strict_types=1);

namespace GameOfLife\LogicCalculator;

use GameOfLife\Organism\Organism;
use GameOfLife\Organism\OrganismList;
use GameOfLife\Organism\OrganismFactory;
use GameOfLife\Organism\SpeciesEnum;
use PHPUnit\Framework\TestCase;

class CellCalculatorTest extends TestCase
{
    /**
     * @dataProvider calculateCellResultProvider
     */
    public function testCalculateCellResult(OrganismList $organismList, int $xPos, int $yPos, ?Organism $organism): void
    {
        $cellCalculator = new CellCalculator();
        $this->assertEquals($cellCalculator->calculateCellResult($organismList, $xPos, $yPos), $organism);
    }

    /**
     * @return string[][]
     */
    public function calculateCellResultProvider(): array
    {
        $speciesRed = SpeciesEnum::create(SpeciesEnum::TYPE_RED);
        $speciesGreen = SpeciesEnum::create(SpeciesEnum::TYPE_GREEN);

        $organismList = new OrganismList([
            OrganismFactory::createOrganism(0, 0, $speciesRed),
            OrganismFactory::createOrganism(1, 0, $speciesRed),
            OrganismFactory::createOrganism(2, 0, $speciesRed),
            OrganismFactory::createOrganism(3, 0, $speciesRed),
            OrganismFactory::createOrganism(5, 0, $speciesRed),
            OrganismFactory::createOrganism(6, 0, $speciesRed),
            OrganismFactory::createOrganism(5, 1, $speciesRed),
            OrganismFactory::createOrganism(6, 1, $speciesGreen),
            OrganismFactory::createOrganism(1, 2, $speciesGreen),
            OrganismFactory::createOrganism(2, 2, $speciesGreen),
            OrganismFactory::createOrganism(3, 2, $speciesGreen),
            OrganismFactory::createOrganism(4, 2, $speciesGreen),
            OrganismFactory::createOrganism(5, 2, $speciesRed),
            OrganismFactory::createOrganism(6, 2, $speciesRed),
        ]);
        return [
            'Result empty cell stay' => [$organismList, 0, 1, null],
            'Result full cell stay' => [$organismList, 1, 0, OrganismFactory::createOrganism(1,0,$speciesRed)],
            'Result born red' => [$organismList, 1, 1, OrganismFactory::createOrganism(1,1,$speciesRed)],
            'Result born green' => [$organismList, 3, 1, OrganismFactory::createOrganism(3,1,$speciesGreen)],
            'Result death overcrowd' => [$organismList, 5, 1, null],
            'Result death isolation' => [$organismList, 6, 1, null],
            //TODO otestovat nejak coord 1,2 na nahodny vysledek?
        ];
    }
}